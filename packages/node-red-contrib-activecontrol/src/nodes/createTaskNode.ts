import { NodeAPI } from "node-red"
import {
  mergeOptions,
  ActiveControlSoapClient
} from "@basistechnologies/activecontrol-soap-api"
import { AcConfigNode } from "../ac_config_definitions"
import { errorText } from "./util"
import {
  CreateTaskNode,
  CreateTaskNodeDef,
  isCreateTaskMessage
} from "../createTaskDefinitions"

module.exports = (RED: NodeAPI) => {
  function createCreateTaskNode(
    this: CreateTaskNode,
    nodeDef: CreateTaskNodeDef
  ) {
    const node = this

    // Create node based on the passed instance definition
    RED.nodes.createNode(node, nodeDef)

    // Get the AC configuration and apply it
    node.activecontrol = RED.nodes.getNode(
      nodeDef.activecontrol
    ) as AcConfigNode

    // check if a config node is given and valid
    if (!node.activecontrol) throw new Error("Configuration node missing")
    if (!node.activecontrol.config || !node.activecontrol.credentials)
      throw new Error("Configuration node not set up properly")

    node.on("input", async function (msg, send, done) {
      try {
        node.status({ fill: "blue", text: "Creating a task..." })

        if (!isCreateTaskMessage(msg)) throw new Error( "Message format not recognized")

        // Check if options were overridden in this node and merge them
        const { url, password, systemnumber, username } = msg
        const config = {
          ...node.activecontrol!.config!,
          ...node.activecontrol!.credentials
        }
        const options = mergeOptions(config, {
          url,
          password,
          systemnumber,
          username
        })

        // Make the call
        const taskClient = new ActiveControlSoapClient(options)
        const taskId = await taskClient.createTask(msg.payload)

        send({ ...msg, payload: taskId })
        node.status("")
        done()
      } catch (error) {
        node.status({ fill: "red", text: errorText(error) })
        done(error)
      }
    })
  }
  RED.nodes.registerType("create-task", createCreateTaskNode)
}

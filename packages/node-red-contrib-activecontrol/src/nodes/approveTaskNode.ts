import { NodeAPI } from "node-red"
import {
  mergeOptions,
  ActiveControlSoapClient,
  isApproveTaskRequest,
  isApproveStepRequest,
  ApproveStepResponse
} from "@basistechnologies/activecontrol-soap-api"
import { AcConfigNode } from "../ac_config_definitions"
import { errorText } from "./util"

import {
  Options,
  ApproveStepRequest,
  ApproveTaskRequest
} from "@basistechnologies/activecontrol-soap-api"
import { NodeDef, Node, NodeMessage } from "node-red"

export interface ApproveTaskNodeDef extends NodeDef {
  activecontrol: string
  throwonfailure: boolean
}

export interface ApproveTaskNode extends Node {
  activecontrol?: AcConfigNode
  throwonfailure: boolean
}

export interface ApproveTaskNodeMessage extends NodeMessage, Partial<Options> {
  payload: ApproveStepRequest | ApproveTaskRequest
}

export function isApproveTaskMessage(
  msg: NodeMessage
): msg is ApproveTaskNodeMessage {
  const { payload } = msg
  return (
    !!payload &&
    (isApproveTaskRequest(payload) || isApproveStepRequest(payload))
  )
}

const throwOnFailure = (x: ApproveStepResponse) => {
  const e = x.messages.find(m => m.Msgtyp.match(/[EAX]/))
  if (e) throw new Error(e.Message)
}

module.exports = (RED: NodeAPI) => {
  function createApproveTaskNode(
    this: ApproveTaskNode,
    nodeDef: ApproveTaskNodeDef
  ) {
    const node = this

    // Create node based on the passed instance definition
    RED.nodes.createNode(node, nodeDef)
    node.throwonfailure = nodeDef.throwonfailure
    // Get the AC configuration and apply it
    node.activecontrol = RED.nodes.getNode(
      nodeDef.activecontrol
    ) as AcConfigNode

    // check if a config node is given and valid
    if (!node.activecontrol) throw new Error("Configuration node missing")
    if (!node.activecontrol.config || !node.activecontrol.credentials)
      throw new Error("Configuration node not set up properly")

    node.on("input", async function (msg, send, done) {
      try {
        node.status({ fill: "blue", text: "Approving task..." })

        if (!isApproveTaskMessage(msg))
         
          throw new Error("Message format not recognized")

        // Check if options were overridden in this node and merge them
        const { url, password, systemnumber, username, payload } = msg
        const config = {
          ...node.activecontrol!.config!,
          ...node.activecontrol!.credentials
        }
        const options = mergeOptions(config, {
          url,
          password,
          systemnumber,
          username
        })

        // Make the call
        const taskApprover = new ActiveControlSoapClient(options)
        if (isApproveStepRequest(payload)) {
          const result = await taskApprover.approveStep(payload)
          if (node.throwonfailure) throwOnFailure(result)
          send({ ...msg, payload: result })
        } else {
          const tresult = await taskApprover.approveTask(payload)
          send({ ...msg, payload: tresult })
        }
        node.status("")
        done()
      } catch (error) {
        node.status({ fill: "red", text: errorText(error) })
        done(error)
      }
    })
  }
  RED.nodes.registerType("approve-task", createApproveTaskNode)
}

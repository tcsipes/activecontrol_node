import {
  Options,
  ReadTaskTransportsInputs
} from "@basistechnologies/activecontrol-soap-api"
import { Node, NodeDef, NodeMessage } from "node-red"
import { AcConfigNode } from "./ac_config_definitions"

export interface ReadTaskTransportsNodeDef extends NodeDef {
  activecontrol: string
}

export interface ReadTaskTransportsNode extends Node {
  activecontrol?: AcConfigNode
}

export interface ReadTaskTransportsNodeMessage
  extends NodeMessage,
    Partial<Options> {
  payload: ReadTaskTransportsInputs
}

export function isReadTaskTransportsMessage(
  msg: NodeMessage
): msg is ReadTaskTransportsNodeMessage {
  return (
    !!msg?.payload &&
    ("XTaskid" in (msg as any).payload ||
      "XTaskReference" in (msg as any).payload)
  )
}

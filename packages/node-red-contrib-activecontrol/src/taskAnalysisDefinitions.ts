import {
  Options,
  StartAnalysisRequest,
  AnalysisStatusRequest,
  isStartAnalysisRequest,
  isAnalysisStatusRequest,
  mergeOptions
} from "@basistechnologies/activecontrol-soap-api"
import { NodeDef, Node, NodeMessage } from "node-red"
import { AcConfigNode } from "./ac_config_definitions"

export interface AnalysisOptions extends Options {
  maxWaitResults: string
  maxLines: string
}

export interface TaskAnalysisNodeDef extends NodeDef {
  activecontrol: string
  maxWaitResults: string
  maxLines: string
}

export interface TaskAnalysisNode extends Node {
  activecontrol?: AcConfigNode
  maxWaitResults: string
  maxLines: string
}

export interface TaskAnalysisNodeMessage
  extends NodeMessage,
    Partial<AnalysisOptions> {
  payload: StartAnalysisRequest | AnalysisStatusRequest
}

export function isTaskAnalysisMessage(
  msg: NodeMessage
): msg is TaskAnalysisNodeMessage {
  return (
    isAnalysisStatusRequest(msg.payload) || isStartAnalysisRequest(msg.payload)
  )
}

export function mergeAnalysisOptions(
  rawOptions: AnalysisOptions,
  incomingOptions: Partial<AnalysisOptions>
): AnalysisOptions {
  const { maxWaitResults, maxLines, ...rest } = incomingOptions
  const ml = undefined === maxLines ? rawOptions.maxLines : maxLines
  const mr =
    undefined === maxWaitResults ? rawOptions.maxWaitResults : maxWaitResults
  const result = {
    ...mergeOptions(rawOptions, rest),
    maxLines: ml || "",
    maxWaitResults: mr || ""
  }
  return result
}

import {
  Options,
  TaskChangeData
} from "@basistechnologies/activecontrol-soap-api"
import { NodeDef, Node, NodeMessage } from "node-red"
import { AcConfigNode } from "./ac_config_definitions"

export interface ChangeTaskNodeDef extends NodeDef {
  activecontrol: string
}

export interface ChangeTaskNode extends Node {
  activecontrol?: AcConfigNode
}
export interface ChangeTaskNodeMessage extends NodeMessage, Partial<Options> {
  payload: TaskChangeData
}
/**
 * check if message is a valid task change message
 * @param msg a node message
 *
 * as most fields are optional, we only require that payload has
 * either an ID or a reference
 * server will complain if mandatory fields are missing
 */
export function isChangeTaskMessage(
  msg: NodeMessage
): msg is ChangeTaskNodeMessage {
  const {
    payload: {
      XTask,
      XUpdateCustfields,
      XUpdateDesc,
      XUpdateTesters,
      XUpddateTask
    } = {
      XTask: {},
      XUpdateCustfields,
      XUpdateDesc,
      XUpdateTesters,
      XUpddateTask
    }
  } = msg as any
  const updateSomething =
    XUpdateCustfields || XUpdateDesc || XUpdateTesters || XUpddateTask
  return (updateSomething && "REFERENCE" in XTask) || "ID" in XTask
}

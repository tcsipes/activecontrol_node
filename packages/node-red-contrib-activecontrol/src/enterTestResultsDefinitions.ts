import { NodeDef, Node, NodeMessage } from "node-red"
import {
  Options,
  TestResultsInputs
} from "@basistechnologies/activecontrol-soap-api"
import { AcConfigNode } from "./ac_config_definitions"

export interface TestResultsNodeDef extends NodeDef {
  activecontrol: string
}

export interface TestResultsNode extends Node {
  activecontrol?: AcConfigNode
}

export interface TestResultsNodeMessage extends NodeMessage, Partial<Options> {
  payload: TestResultsInputs
}

export const isTestResultsMessage = (
  msg: NodeMessage
): msg is TestResultsNodeMessage => {
  const pl: any = msg.payload || {}
  const {} = pl
  return !!pl.XTaskid && pl.XTarget && pl.XRescode
}

export function validateTestResultsInputs(inputs: TestResultsInputs) {
  if (!inputs.XRescode?.match(/^\d$/))
    throw new Error("Invalid test result code, should be a single digit number")
  if (!inputs.XTaskid?.match(/^[\d]+$/)) throw new Error("Invalid Task id")
  if (!inputs.XTarget?.match(/^\d\d\d\d$/)) throw new Error("Invalid Target ID")
}

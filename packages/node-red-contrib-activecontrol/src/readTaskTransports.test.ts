import { config } from "dotenv"
import helper from "node-red-node-test-helper"
import { env } from "process"
import { ReadTaskTransportsNode } from "./readTaskTransportsDefinitions"
const acConfig = require("./nodes/ac_config_node")
const readTaskTransportsNode = require("./nodes/readTaskTransportsNode")

config()
const readConfigFromEnv = () => ({
  url: env.AC_SERVER || "",
  username: env.AC_USER,
  password: env.AC_PASSWORD,
  systemnumber: env.AC_SYSTEM_NUMBER || ""
})

beforeEach(done => helper.startServer(done))
afterEach(done => {
  helper.unload()
  helper.stopServer(done)
})

test("Node registration", done => {
  const flow = [
    {
      id: "read",
      type: "read-task-transports",
      name: "readTaskTransports",
      activecontrol: "ac",
      wires: [["n2"]]
    },
    { id: "ac", type: "ac_config", name: "test name", url: "http://foo" },
    { id: "n2", type: "helper" }
  ]
  helper.load([acConfig, readTaskTransportsNode], flow, function () {
    const n2 = helper.getNode("read") as ReadTaskTransportsNode

    expect(n2).toBeDefined()
    expect(n2.name).toBe("readTaskTransports")
    expect(n2.activecontrol?.id).toBe("ac")
    expect(n2.activecontrol?.config?.url).toBe("http://foo")
    done()
  })
})

test("Parameter Task ID supplied", async done => {
  const { url, systemnumber, username, password } = readConfigFromEnv()
  const flow = [
    {
      id: "read",
      type: "read-task-transports",
      name: "readTaskTransports",
      activecontrol: "ac",
      wires: [["n2"]]
    },
    {
      id: "ac",
      type: "ac_config",
      name: "test name",
      url,
      systemnumber
    },
    { id: "n2", type: "helper" }
  ]

  const result: any = await new Promise(resolve => {
    helper.load(
      [acConfig, readTaskTransportsNode],
      flow,
      { ac: { username, password } },
      function () {
        const reader = helper.getNode("read") as ReadTaskTransportsNode
        const receiver = helper.getNode("n2") as ReadTaskTransportsNode

        expect(reader).toBeDefined()
        expect(reader.name).toBe("readTaskTransports")

        receiver.on("input", function (msg) {
          resolve(msg)
        })
        reader.receive({ payload: { XTaskid: env.AC_TASKID } })
      }
    )
  })
  const response: any = result?.payload
  expect(response).toHaveProperty("taskIds")
  expect(response.taskIds).toHaveLength(1)
  expect(response.taskIds[0].Id).toBe(env.AC_TASKID)
  done()
})

test("Parameter Task Reference supplied", async done => {
  const { url, systemnumber, username, password } = readConfigFromEnv()
  const flow = [
    {
      id: "read",
      type: "read-task-transports",
      name: "readTaskTransports",
      activecontrol: "ac",
      wires: [["n2"]]
    },
    {
      id: "ac",
      type: "ac_config",
      name: "test name",
      url,
      systemnumber
    },
    { id: "n2", type: "helper" }
  ]

  const result: any = await new Promise(resolve => {
    helper.load(
      [acConfig, readTaskTransportsNode],
      flow,
      { ac: { username, password } },
      function () {
        const reader = helper.getNode("read") as ReadTaskTransportsNode
        const receiver = helper.getNode("n2") as ReadTaskTransportsNode

        expect(reader).toBeDefined()
        expect(reader.name).toBe("readTaskTransports")

        receiver.on("input", function (msg) {
          resolve(msg)
        })
        reader.receive({ payload: { XTaskReference: env.AC_TASK_REFERENCE } })
      }
    )
  })
  const response: any = result?.payload
  expect(response).toHaveProperty("taskIds")
  expect(response.taskIds).toHaveLength(1)
  expect(response.taskIds[0].Id).toBe(env.AC_TASKID)
  done()
})

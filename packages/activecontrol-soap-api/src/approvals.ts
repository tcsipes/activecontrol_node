import { decode } from "he"
import { AnalysisResults, parseAnalysisResults } from "./analysis"
import { composeXml, xmlArray, xmlSubNode } from "./soap"
import { ReturnMessage, TargetLocation } from "./taskDefinitions"
import {
  isValidApprLocation, isValidId, isValidReference, isValidRequest, isValidTarget
} from "./utils"

export enum AnalysisMode {
  None = "N",
  Force = "F",
  StopOnError = " "
}
export interface ApproveTaskRequest {
  XAnalysisId: string
  XLocation: TargetLocation
  XTarget: string
  XTaskid: string
  XtRequest: { Trkorr: string }[]
}

export interface ApproveStepByReferenceRequest {
  XReference: string
  XStep: string
  XAnalysisMode: AnalysisMode
}

export interface ApproveStepByIdRequest {
  XTaskid: string
  XStep: string
  XAnalysisMode: AnalysisMode
}

export type ApproveStepRequest =
  | ApproveStepByIdRequest
  | ApproveStepByReferenceRequest

export const isAnalysisMode = (x: any): x is AnalysisMode =>
  x === AnalysisMode.Force ||
  x === AnalysisMode.None ||
  x === AnalysisMode.StopOnError

const isValidStep = isValidReference
export const isApproveTaskRequest = (x: any): x is ApproveTaskRequest => {
  const {
    XAnalysisId = "",
    XLocation = "",
    XTarget = "",
    XTaskid = "",
    XtRequest = []
  } = x || {}
  if (
    isValidId(XAnalysisId) &&
    isValidId(XTaskid) &&
    isValidTarget(XTarget) &&
    isValidApprLocation(XLocation)
  ) {
    for (const tr of XtRequest) if (!isValidRequest(tr?.Trkorr)) return false
    return true
  }
  return false
}

export const isApproveStepByRefReq = (
  x: any
): x is ApproveStepByReferenceRequest => {
  const {
    XSystemNumber = "",
    XReference = "",
    XStep = "",
    XAnalysisMode = ""
  } = x || {}
  if (
    isValidReference(XReference) &&
    isAnalysisMode(XAnalysisMode) &&
    isValidStep(XStep)
  )
    return true
  return false
}

export const isApproveStepByIdReq = (x: any): x is ApproveStepByIdRequest => {
  const { XTaskid = "", XStep = "", XAnalysisMode = "" } =
    x || {}
  if (
    isValidId(XTaskid) &&
    isAnalysisMode(XAnalysisMode) &&
    isValidStep(XStep)
  )
    return true
  return false
}

export const isApproveStepRequest = (x: any): x is ApproveStepRequest =>
  isApproveStepByIdReq(x) || isApproveStepByRefReq(x)

  export const approveTaskRequestToXml = (
    request: ApproveTaskRequest,
    XSystemNumber: string
  ) => {
    const { XtRequest, ...rest } = request
    const transports = XtRequest.map(
      tr => `<item><Trkorr>${tr.Trkorr}</Trkorr></item>`
    )
    return composeXml(
      [
        "XAnalysisId",
        "XLocation",
        "XSystemNumber",
        "XTarget",
        "XTaskid",
        "XtRequest"
      ],
      { ...rest,  XSystemNumber, XtRequest: transports.join("") }
    )
  }
export const approveStepRequestToXml = (request: ApproveStepRequest,XSystemNumber:string) => {
  const bo = {
    XReference: "",
    YtConflictRequests: "",
    XSystemNumber,
    YtMessages: "",
    YtResults: "",
    ...request
  }
  return composeXml(
    [
      "XAnalysisMode",
      "XReference",
      "XStep",
      "XSystemNumber",
      "YtConflictRequests",
      "YtMessages",
      "YtResults"
    ],
    bo
  )
}

export const parseApproveTaskResponse = (r: any) => {
  const message = xmlSubNode(r, "YReturn")
  return message as ReturnMessage
}
  
export const parseApproveStepResponse = (r: any) => {
  const rawanalysis = xmlSubNode(r, "YAnalysisResults")
  const conflicts = xmlArray(r, "YtConflictRequests", "item").map(x => ({
    ...x,
    AllowConfirm: x.AllowConfirm === "X",
    AllowAdminConf: x.AllowAdminConf === "X",
    AllowImport: x.AllowImport === "X"
  }))
  const messages = xmlArray(r, "YtMessages", "item")
  const results = xmlArray(r, "YtResults", "item").map(x => ({
    ...x,
    Problems: x.Problems === "X",
    Canapprove: x.Canapprove === "X",
    Approved: x.Approved === "X"
  }))
  const analysis = parseAnalysisResults(decode(rawanalysis))
  return { analysis, conflicts, messages, results } as ApproveStepResponse
}
export interface ApproveStepResponse {
  analysis: AnalysisResults[]
  conflicts: ApproveStepConflict[]
  messages: ReturnMessage[]
  results: ApproveStepResult[]
}

export interface ApproveStepConflict {
  Strkorr: string
  Reason: string
  Trkorr: string
  Reldate: string
  Reltime: string
  AllowConfirm: boolean
  AllowAdminConf: boolean
  AllowImport: boolean
}

export interface ApproveStepResult {
  Task: string
  Reference: string
  Target: string
  Location: TargetLocation
  Problems: boolean
  Canapprove: boolean
  Approved: boolean
}

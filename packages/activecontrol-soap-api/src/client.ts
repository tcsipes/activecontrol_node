import axios, { AxiosRequestConfig } from "axios"
import { parse } from "fast-xml-parser"
import {
  AnalysisStatusRequest,
  analysisStatusRequestToXml,
  isAnalysisStatusRequest,
  isStartAnalysisRequest,
  parseReadAnalysisResult,
  parseStartAnalysisResponse,
  StartAnalysisRequest,
  startAnalysisRequestToXml
} from "./analysis"
import {
  ApproveStepRequest,
  approveStepRequestToXml,
  ApproveTaskRequest,
  approveTaskRequestToXml,
  isApproveStepRequest,
  isApproveTaskRequest,
  parseApproveStepResponse,
  parseApproveTaskResponse
} from "./approvals"
import { Options, validateOptions } from "./options"
import {
  extractErrorMessage,
  formatAsXml,
  stripEnvelope,
  wrapInEnvelope
} from "./soap"
import {
  isReturnMessage,
  messageIsError,
  ReadTaskInputs,
  ReadTaskTransportsInputs,
  ReadTaskTransportsResponse,
  Task,
  TaskChangeData,
  TestResultsInputs,
  validateReadTaskInputs,
  validateReadTaskTransportsInputs,
  validateTestResultsInputs
} from "./taskDefinitions"
import {
  readTaskTransportsFromXml,
  readTaskTransportsToXml,
  taskChangeToXml,
  taskCreateToXml,
  taskfromXml
} from "./utils"

export class ActiveControlSoapClient {
  constructor(private options: Options) {
    validateOptions(options)
  }

  public async readTask(
    readTaskInputs: ReadTaskInputs,
    options: AxiosRequestConfig = {}
  ) {
    validateReadTaskInputs(readTaskInputs)
    try {
      const data = this.createTaskReadRequest(readTaskInputs)
      const raw = await this.request(
        data,
        "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskReadWsRequest",
        "n0:_-bti_-teTaskReadWsResponse",
        options
      )
      return taskfromXml(raw)
    } catch (error) {
      throw new Error("Failed to read an ActiveControl task:" + error.message)
    }
  }

  public async readTaskTransports(
    readTaskTransportsInputs: ReadTaskTransportsInputs,
    options: AxiosRequestConfig = {}
  ): Promise<ReadTaskTransportsResponse> {
    validateReadTaskTransportsInputs(readTaskTransportsInputs)

    const envelope = wrapInEnvelope(
      "urn:_-bti_-teTaskGetTransportsWs",
      readTaskTransportsToXml(readTaskTransportsInputs)
    )

    const response = await this.request(
      envelope,
      "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskGetTransportsWsRequest",
      "n0:_-bti_-teTaskGetTransportsWsResponse",
      options
    )

    return readTaskTransportsFromXml(response)
  }

  public async startAnalysis(
    analysisdet: StartAnalysisRequest,
    options: AxiosRequestConfig = {}
  ) {
    if (!isStartAnalysisRequest(analysisdet))
      throw new Error("Invalid start analysis request")
    const reqData = wrapInEnvelope(
      "urn:_-bti_-teTaskAnalyseWs",
      startAnalysisRequestToXml(this.options.systemnumber, analysisdet)
    )
    const resp = await this.request(
      reqData,
      "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskAnalyseWsRequest",
      "n0:_-bti_-teTaskAnalyseWsResponse",
      options
    )
    return parseStartAnalysisResponse(resp)
  }

  public async readAnalysisResults(
    statusreq: AnalysisStatusRequest,
    options: AxiosRequestConfig = {}
  ) {
    if (!isAnalysisStatusRequest(statusreq))
      throw new Error("Invalid read analysis request")
    const reqData = wrapInEnvelope(
      "urn:_-bti_-teTaskReadAnalysisWs",
      analysisStatusRequestToXml(this.options.systemnumber, statusreq)
    )
    const resp = await this.request(
      reqData,
      "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskReadAnalysisWsRequest",
      "n0:_-bti_-teTaskReadAnalysisWsResponse",
      options
    )
    return parseReadAnalysisResult(resp, statusreq.XAnalysisId)
  }

  /**
   * Approve a task by ID or reference and step
   * Steps are linked to locations via table /BTI/TE_INT_LOC
   *
   * @param stepRequest Approval step request (either by task or by reference)
   * @param options Axios options to custommize the call
   */
  public async approveStep(
    stepRequest: ApproveStepRequest,
    options: AxiosRequestConfig = {}
  ) {
    if (!isApproveStepRequest(stepRequest))
      throw new Error("Invalid task approval request")
    const reqData = wrapInEnvelope(
      "urn:_-bti_-teTaskApproveStepWs",
      approveStepRequestToXml(stepRequest, this.options.systemnumber)
    )
    const resp = await this.request(
      reqData,
      "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskApproveStepWsRequest",
      "n0:_-bti_-teTaskApproveStepWsResponse",
      options
    )
    return parseApproveStepResponse(resp)
  }

  /**
   * @deprecated as doesn't check if the approval is valid - use approveStep instead
   *
   * Approve a business task in a given location
   *
   * @param taskRequest
   * @param options
   */
  public async approveTask(
    taskRequest: ApproveTaskRequest,
    options: AxiosRequestConfig = {}
  ) {
    if (!isApproveTaskRequest(taskRequest))
      throw new Error("Invalid task approval request")
    const reqData = wrapInEnvelope(
      "urn:_-bti_-teTaskApproveWs",
      approveTaskRequestToXml(taskRequest, this.options.systemnumber)
    )
    const resp = await this.request(
      reqData,
      "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskApproveWsRequest",
      "n0:_-bti_-teTaskApproveWsResponse",
      options
    )
    const msg = parseApproveTaskResponse(resp)
    if (messageIsError(msg)) throw new Error(msg.Message)
    return { message: msg.Message }
  }

  public async createTask(
    task: Partial<Task>,
    options: AxiosRequestConfig = {}
  ) {
    try {
      const taskData = wrapInEnvelope(
        "urn:_-bti_-teTaskCreateWs",
        taskCreateToXml(this.options.systemnumber, task)
      )

      const { YReturn, YTask } = await this.request(
        taskData,
        "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskCreateWsRequest",
        "n0:_-bti_-teTaskCreateWsResponse",
        options
      )
      if (isReturnMessage(YReturn)) {
        if (messageIsError(YReturn)) throw new Error(YReturn.Message)
        const taskId = `${YTask?.Id || YReturn?.Msgv1}`
        if (taskId.match(/\d+/)) return taskId
      }
    } catch (error) {
      throw new Error("Failed to create an ActiveControl task:" + error.message)
    }
    throw new Error(
      "Failed to create an ActiveControl task - unexpected response"
    )
  }

  public async changeTask(
    task: TaskChangeData,
    options: AxiosRequestConfig = {}
  ) {
    try {
      const changeTaskData = wrapInEnvelope(
        "urn:_-bti_-teTaskChangeWs",
        taskChangeToXml(this.options.systemnumber, task)
      ) // should be the xml representation of a request
      const { YReturn } = await this.request(
        changeTaskData,
        "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskChangeWsRequest",
        "n0:_-bti_-teTaskChangeWsResponse",
        options
      )
      if (isReturnMessage(YReturn)) {
        if (messageIsError(YReturn)) throw new Error(YReturn.Message)
        return {
          message: "Task changed successfully"
        }
      }
    } catch (error) {
      throw new Error("Failed to change an ActiveControl task:" + error.message)
    }
    throw new Error(
      "Failed to change an ActiveControl task - unexpected response"
    )
  }

  public async enterTestResults(
    testResultsInputs: TestResultsInputs,
    options: AxiosRequestConfig = {}
  ) {
    try {
      validateTestResultsInputs(testResultsInputs)
      const data = this.createTestResultRequest(testResultsInputs)

      const { YReturn } = await this.request(
        data,
        "urn:sap-com:document:sap:soap:functions:mc-style:_-BTI_-TE_TASK_WS:_-bti_-teTaskReadWsRequest",
        "n0:_-bti_-teTaskTestresWsResponse",
        options
      )
      if (isReturnMessage(YReturn)) {
        if (messageIsError(YReturn)) throw new Error(YReturn.Message)
        return {
          message: YReturn.Message,
          resultsId: YReturn.Msgv1
        }
      }
    } catch (error) {
      throw new Error("Failed to enter a test result:" + error.message)
    }
    throw new Error("Failed to enter a test result - unexpected response")
  }

  private createTestResultRequest(inputs: TestResultsInputs) {
    const XClose = inputs.XClose ? "X" : ""
    const data = {
      ...inputs,
      XSystemNumber: this.options.systemnumber,
      XLocation: "T",
      XClose
    }
    const payload = formatAsXml(
      [
        "XClose",
        "XComment",
        "XLocation",
        "XRescode",
        "XSystemNumber",
        "XTarget",
        "XTaskid"
      ],
      data
    )
    return wrapInEnvelope("urn:_-bti_-teTaskTestresWs", payload)
  }

  private async request(
    data: any,
    SOAPAction: string,
    resultRoot: string,
    options: AxiosRequestConfig
  ) {
    const Authorization = this.createAuth()
    const headers = {
      "user-agent": "ActiveControl",
      "Content-Type": "text/xml;charset=UTF-8",
      SOAPAction,
      Authorization,
      ...options.headers
    }
    try {
      const response = await axios(this.options.url, {
        ...options,
        headers,
        method: "post",
        data
      })
      const raw = parse(response.data, { parseTrueNumberOnly: true })
      return stripEnvelope(raw, resultRoot)
    } catch (error) {
      throw new Error(extractErrorMessage(error))
    }
  }
  private createAuth() {
    if (this.options.authorization) return this.options.authorization
    const { username, password } = this.options
    const encoded = Buffer.from(`${username}:${password}`).toString("base64")
    return `Basic ${encoded}`
  }
  private createTaskReadRequest(inputs: ReadTaskInputs) {
    const data = {
      XSystemNumber: this.options.systemnumber,
      ...inputs
    }
    const payload = formatAsXml(["XSystemNumber", "XTaskid"], data)
    return wrapInEnvelope("urn:_-bti_-teTaskReadWs", payload)
  }
}

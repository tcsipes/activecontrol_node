import { Client, Settings } from "./client"
import * as library from "./library"
import * as projects from "./projects"
import * as runtimeSettings from "./runtimeSettings"
import * as sessions from "./sessions"

export function init(settings: Partial<Settings>) {
  /* To debug what node-RED passes to the API,
  import the object "runtime" as the 2nd parameter
  and use runtime.log.trace("") to write to the console */

  const client = new Client(settings)

  projects.projects.client = client
  runtimeSettings.runtimeSettings.client = client
  sessions.sessions.client = client
  library.library.client = client
}

export * from "./projects"
export * from "./library"
export * from "./runtimeSettings"
export * from "./sessions"

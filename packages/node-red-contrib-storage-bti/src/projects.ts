import { Client } from "./client"

class Projects {
  private _client: Client | undefined

  set client(client: Client) {
    this._client = client
  }

  public async getFlows() {
    return this._client?.performGetRequest("/storage/flows").then(response => {
      if (response) return response.data
      else return []
    })
  }

  public async saveFlows(flows: object) {
    return this._client
      ?.performPutRequest("/storage/flows", flows)
      .then(response => response.statusText)
  }

  public async getCredentials() {
    return this._client
      ?.performGetRequest("/storage/credentials")
      .then(response => {
        if (response) return response.data
        else return []
      })
  }

  public async saveCredentials(credentials: object) {
    return this._client
      ?.performPutRequest("/storage/credentials", credentials)
      .then(response => response.statusText)
  }
}

export const projects: Projects = new Projects()

export async function getFlows() {
  return projects.getFlows()
}
export async function saveFlows(flows: object) {
  return projects.saveFlows(flows)
}
export async function getCredentials() {
  return projects.getCredentials()
}
export async function saveCredentials(credentials: object) {
  return projects.saveCredentials(credentials)
}

import { Client } from "./client"

class Sessions {
  private _client: Client | undefined

  set client(client: Client) {
    this._client = client
  }

  public async getSessions() {
    return this._client
      ?.performGetRequest("/storage/sessions")
      .then(response => {
        if (response) return response.data
        else return []
      })
  }

  public async saveSessions(sessions: object) {
    return this._client
      ?.performPutRequest("/storage/sessions", sessions)
      .then(response => response.statusText)
  }
}

export const sessions: Sessions = new Sessions()

export async function getSessions() {
  return sessions.getSessions()
}
export async function saveSessions(s: object) {
  return sessions.saveSessions(s)
}

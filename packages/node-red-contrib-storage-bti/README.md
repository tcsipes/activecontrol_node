[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://bitbucket.org/basistech/activecontrol_node)
# Basis Technologies Storage API implementation for Node-RED
This plugin provides a configurable way of storing your [Node-RED](https://nodered.org/) runtime on an SAP® server instance. 

The default storage location for Node-RED data is the local file-system. This module implements the Node-RED [Storage API](https://nodered.org/docs/api/storage/) that communicates with the backend.

## Usage
- Make sure your backend has the necessary Node-RED API components from Basis Technologies installed.
- Install Node-RED and install this module via npm
- Edit the settings.js file of your Node-RED installation (Note: in the dev environment, create the file under `/data`). The Node-RED runtime requires the following configuration:
```js
storageModule: require("@basistechnologies/node-red-contrib-storage-bti"),
storageBaseUrl: "https://<server>:<port>", // the forward slash after the port is optional
storageUsername: "user",
storagePassword: "password",
//storageAuthorization: "Basic dGVzdDp0ZXN0"
```
- You can choose to either provide username+password or an authorization string that will be used as the HTTP Authorization request header.